#include <iostream>
#include <fstream>
#include <assert.h> 
using namespace std;


int main(int argc, char** argv){
	assert(argc >= 2 && "Expecting at least one command-line argument.");
	ifstream input(argv[1]);
	assert(input && "Failed to open input stream.");
	
	for(string line; getline(input, line);){
		int marker = line.find('|');
		int bugs = 0;
		for (int i=0;i<(marker-1);i++){
			if(line[i]!=line[i+marker+2]) bugs++;
		}
		string priority;
		if(bugs==0) priority = "Done";
		else if(bugs<=2) priority = "Low";
		else if(bugs<=4) priority = "Medium";
		else if(bugs<=6) priority = "High";
		else priority = "Critical";
		cout << priority << endl;
	}

}