#include <iostream>
#include <fstream>
using namespace std;

bool isPrime(int number){
    if(number < 2) return false;
    if(number == 2) return true;
    if(number % 2 == 0) return false;
    for(int i=3; (i*i)<=number; i+=2){
        if(number % i == 0 ) return false;
    }
    return true;
}

bool isPalindrome(int number){
    int digit,rev = 0;
    int n=number;
    do
    {
        digit = number%10;
        rev = (rev*10) + digit;
        number = number/10;
    }while (number!=0);

    if (n==rev)
        return true;
    else
        return false;
}

int main() {

    for (int i=1000;i>0;i--) {
        if(isPrime(i) & isPalindrome(i)){
            cout << i <<endl;
            break;
        }
        else
            continue;
    }
    return 0;
} 