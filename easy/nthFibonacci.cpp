#include <iostream>
#include <fstream>
#include <assert.h> 
using namespace std;

int fib(int n){
	if (n == 1 || n==0)
		return n;
	return fib(n-1) + fib(n-2);
}

int main(int argc, char** argv){
	assert(argc >= 2 && "Expecting at least one command-line argument.");
	ifstream input(argv[1]);
	assert(input && "Failed to open input stream.");

	for(int x = 0;input >> x;){
		cout << fib(x) << endl;
	}
}