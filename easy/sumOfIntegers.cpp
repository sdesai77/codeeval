#include <iostream>
#include <fstream>
#include <istream>
#include <assert.h> 
using namespace std;

int main(int argc, char** argv){
	assert(argc >= 2 && "Expecting at least one command-line argument.");
	ifstream input(argv[1]);
	assert(input && "Failed to open input stream.");

	int sum=0;
 	for(int x = 0; input >> x;){
		sum+=x;
	}
	cout << sum << endl;
}	