#include <iostream>

using namespace std;

bool isPrime(int number){
    if(number < 2) return false;
    if(number == 2) return true;
    if(number % 2 == 0) return false;
    for(int i=3; (i*i)<=number; i+=2){
        if(number % i == 0 ) return false;
    }
    return true;
}


int main() {
    int i=1,count,sum=0;
    while(true){
        i++;
        if(isPrime(i)){
            sum+=i;
            count++;
        }
        if(count == 1000)
            break;
    }
    cout << sum << endl;
    return 0;
} 