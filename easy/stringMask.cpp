#include <cassert>
#include <fstream>
#include <iostream>
using namespace std;

string maskWord (string word, string mask){
	for (int i=0;i<mask.length();i++){
		if(mask[i]=='1'){
			word[i]=word[i]-32; 
		}
	}
	return word;
	
}

int main(int argc, const char* argv[]){
	assert(argc >= 2 && "Expecting at least one command-line argument.");
	ifstream input(argv[1]);
	assert(input && "Failed to open input stream.");

 	for(string word,mask; input >> word >> mask;){
		assert(word.size() == mask.size());
		//cout << word << '\t' << mask << endl;	
		string result = maskWord(word,mask);
		cout << result << endl;
	}
}	