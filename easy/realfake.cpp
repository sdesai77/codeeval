//https://www.codeeval.com/open_challenges/227/

#include <iostream>
#include <fstream>
#include <string.h>
using namespace std;

void removeSpaces(string& str){
    int len = str.length();
    int j = 0;
    for (int i = 0; i < len;){
		if (str.at(i) == ' '){
            i++;
            continue;
        }
        str.at(j++) = str.at(i++);
    }
    str.resize(j);
}

int main(int argc, char** argv){
	ifstream file(argv[1]);
    string line;

    while (getline(file, line)){
		string x = line;
		removeSpaces(x);
		int sum=0;
		for (int i=0; i < x.length(); i++){
			if (i%2==0){
				sum += (x[i]-'0')*2;
			} 
			else sum += x[i]-'0';
		}

        if(sum%10==0)
            cout<<"Real"<<endl;
        else
            cout<<"Fake"<<endl;
    }
}