#include <iostream>
#include <fstream>
#include <assert.h> 
using namespace std;

int main(int argc, char** argv){
	assert(argc >= 2 && "Expecting at least one command-line argument.");
	ifstream input(argv[1]);
	assert(input && "Failed to open input stream.");

	for(int x = 0, y = 0, n = 0; input >> x >> y >> n;){
		for(int i=1; i<=n; i++){
			bool isF = !(i%x);
			bool isB = !(i%y);
			
			if (isF && isB) cout << "FB";
			else if (isF || x==1) cout << "F";
			else if (isB || y==1) cout << "B";
			else cout << i;
			cout << " ";
		}
		cout << "\n";
	}
}