#include <iostream>
#include <fstream>
#include <assert.h> 
using namespace std;

int reverseDigits(int num)
{
    int rev_num = 0;
    while(num > 0)
    {
        rev_num = rev_num*10 + num%10;
        num = num/10;
    }
    return rev_num;
}

int main(int argc, char** argv){
	assert(argc >= 2 && "Expecting at least one command-line argument.");
	ifstream input(argv[1]);
	assert(input && "Failed to open input stream.");

	for(int x = 0;input >> x;){
		int sum = x + reverseDigits(x); int count =1;
		
		while(sum!=reverseDigits(sum)){
			count++;
			sum += reverseDigits(sum);			
		}
		cout << count << " " << sum << endl;
	}
}